import exercise.percentile_rank.io.CsvFileReader;
import exercise.percentile_rank.pojo.Student;
import exercise.percentile_rank.service.Percentile;
import org.junit.Assert;
import org.junit.Test;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.List;

import static org.junit.Assert.fail;

public class TestPercentileRanksFromFiles {

    private File getFile(String fileName) throws FileNotFoundException {
        ClassLoader classLoader = getClass().getClassLoader();
        if (classLoader.getResource(fileName) == null) {
            throw new FileNotFoundException("File not exist");
        } else {
            String url = classLoader.getResource(fileName).getFile();
            if (url == null) {
                throw new FileNotFoundException("File not exist");
            }
            return new File(url);
        }
    }

    @Test
    public void testFileDoesNotExist() {
        //does not exist
        try {
            getFile("test_invalid0.csv");
            fail("File does not exist");
        } catch (FileNotFoundException ignored) {
        }
    }

    @Test
    public void testFileInvalid1() {
        File file;
        try {
            file = getFile("test_invalid1.csv");
            try {
                new CsvFileReader().readCsvFile(file);
                fail("My method didn't throw when I expected it to");
            } catch (Exception ignored) {
            }
        } catch (FileNotFoundException ignored) {
        }

    }


    @Test
    public void testFile1() throws Exception {
        File file = getFile("test1.csv");

        Student student = new Student("909401US", "\"Maria Russell\"", 21);
        List<Student> studentList = new CsvFileReader().readCsvFile(file);
        Percentile percentile = new Percentile();
        percentile.init(studentList);

        Assert.assertEquals(percentile.rank(student), 50.00, 0.01);
    }

    @Test
    public void testFile2() throws Exception {
        File file = getFile("test2.csv");

        Student student = new Student("97579US", "\"Bruce Nelson\"", 18);
        List<Student> studentList = new CsvFileReader().readCsvFile(file);
        Percentile percentile = new Percentile();
        percentile.init(studentList);

        Assert.assertEquals(percentile.rank(student), 50.00, 0.01);
    }

    @Test
    public void testFile3() throws Exception {
        File file = getFile("test3.csv");

        Student student = new Student("97579US", "\"Irene Simmons\"", 85);
        List<Student> studentList = new CsvFileReader().readCsvFile(file);
        Percentile percentile = new Percentile();
        percentile.init(studentList);

        Assert.assertEquals(percentile.rank(student), 75.00, 0.01);
    }

}