import exercise.percentile_rank.io.CsvFileReader;
import exercise.percentile_rank.io.CsvFileWriter;
import exercise.percentile_rank.pojo.Student;
import exercise.percentile_rank.service.Percentile;
import org.junit.Test;

import java.io.File;
import java.util.List;

public class TestBigFiles {

    @Test
    public void testCreateAndProcessBigFile1K() throws Exception {

        File file = File.createTempFile("test", "big");
        CsvFileWriter csvFileWriter = new CsvFileWriter();
        csvFileWriter.writeCsvFile(file, 1000);
        List<Student> studentList = new CsvFileReader().readCsvFile(file);

        Percentile percentile = new Percentile();
        percentile.init(studentList);

        for (Student i : studentList) {
            System.out.println("Percentile(" + i + "): " + percentile.rank(i));
        }
        file.deleteOnExit();
    }

    @Test
    public void testCreateAndProcessBigFile100K() throws Exception {

        File file = File.createTempFile("test", "big");
        CsvFileWriter csvFileWriter = new CsvFileWriter();
        csvFileWriter.writeCsvFile(file, 100000);
        List<Student> studentList = new CsvFileReader().readCsvFile(file);

        Percentile percentile = new Percentile();
        percentile.init(studentList);

        for (Student i : studentList) {
            System.out.println("Percentile(" + i + "): " + percentile.rank(i));
        }
        file.deleteOnExit();
    }

    @Test
    public void testCreateAndProcessBigFile1M() throws Exception {

        File file = File.createTempFile("test", "big");
        CsvFileWriter csvFileWriter = new CsvFileWriter();
        csvFileWriter.writeCsvFile(file, 1000000);

        List<Student> studentList = new CsvFileReader().readCsvFile(file);

        Percentile percentile = new Percentile();
        percentile.init(studentList);

        for (Student i : studentList) {
            System.out.println("Percentile(" + i + "): " + percentile.rank(i));
        }
        file.deleteOnExit();
    }

}