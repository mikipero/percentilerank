package exercise.percentile_rank.util;

import exercise.percentile_rank.pojo.Student;

import java.util.List;

//implementation of https://en.wikipedia.org/wiki/Binary_search_algorithm
public class StudentListHelper {

    //f x is present in studentList then returns the count of occurrences of x, otherwise returns -1.
    public static int countSameGPA(List<Student> studentList, double x) {
        int n = studentList.size();
        // index of first occurrence of x in list
        int i;
        // index of last occurrence of x in list
        int j;
        //get the index of first occurrence of x
        i = first(studentList, 0, n - 1, x, n);
        //If x doesn't exist in arr[] then return -1
        if (i == -1)
            return i;

        // Else get the index of last occurrence of x. Note that we are only looking in the sub array after first occurrence
        j = last(studentList, i, n - 1, x, n);

        return j - i + 1;
    }

    //f x is present in studentList then returns the count of occurrences of x less than x
    public static int countLessThenGPA(List<Student> studentList, double x) {

        int n = studentList.size();
        // index of first occurrence of x in arr[0..n-1]
        int i;

        //get the index of first occurrence of x
        i = first(studentList, 0, n - 1, x, n);
        //If x doesn't exist in arr[] then return -1
        if (i == -1)
            return i;
        return i;
    }

    //if x is present in arr[] then returns the index of FIRST occurrence of x in arr[0..n-1], otherwise returns -1
    private static int first(List<Student> studentList, int low, int high, double x, int n) {
        if (high >= low) {
            int mid = (low + high) / 2;
            if ((mid == 0 || x > studentList.get(mid - 1).getGPA()) && studentList.get(mid).getGPA() == x)
                return mid;
            else if (x > studentList.get(mid).getGPA())
                return first(studentList, (mid + 1), high, x, n);
            else
                return first(studentList, low, (mid - 1), x, n);
        }
        return -1;
    }

    //if x is present in arr[] then returns the index of LAST occurrence of x in arr[0..n-1], otherwise returns -1
    private static int last(List<Student> studentList, int low, int high, double x, int n) {
        if (high >= low) {
            int mid = (low + high) / 2;
            if ((mid == n - 1 || x < studentList.get(mid + 1).getGPA()) && studentList.get(mid).getGPA() == x)
                return mid;
            else if (x < studentList.get(mid).getGPA())
                return last(studentList, low, (mid - 1), x, n);
            else
                return last(studentList, (mid + 1), high, x, n);
        }
        return -1;
    }

}
