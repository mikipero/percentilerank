package exercise.percentile_rank.util;

import exercise.percentile_rank.pojo.Student;

import java.util.ArrayList;
import java.util.List;

//Class to generate radnom strings, numbers, Students....
public class RandomGenerator {

    final private String ALPHABET = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";

    final private String NUMBERS = "0123456789";

    final private String COUNTRIES[] = {"Afghanistan", "Albania", "Algeria", "Andorra", "Angola", "Antarctica", "Antigua and Barbuda", "Argentina",
            "Armenia", "Australia", "Austria", "Azerbaijan", "Bahamas", "Bahrain", "Bangladesh", "Barbados", "Belarus", "Belgium", "Belize",
            "Benin", "Bermuda", "Bhutan", "Bolivia", "Bosnia and Herzegovina", "Botswana", "Brazil", "Brunei", "Bulgaria", "Burkina Faso",
            "Burma", "Burundi", "Cambodia", "Cameroon", "Canada", "Cape Verde", "Central African Republic", "Chad", "Chile", "China",
            "Colombia", "Comoros", "Congo, Democratic Republic", "Congo, Republic of the", "Costa Rica", "Cote d Ivoire", "Croatia", "Cuba",
            "Cyprus", "Czech Republic", "Denmark", "Djibouti", "Dominica", "Dominican Republic", "East Timor", "Ecuador", "Egypt",
            "El Salvador", "Equatorial Guinea", "Eritrea", "Estonia", "Ethiopia", "Fiji", "Finland", "France", "Gabon", "Gambia", "Georgia",
            "Germany", "Ghana", "Greece", "Greenland", "Grenada", "Guatemala", "Guinea", "Guinea-Bissau", "Guyana", "Haiti", "Honduras",
            "Hong Kong", "Hungary", "Iceland", "India", "Indonesia", "Iran", "Iraq", "Ireland", "Israel", "Italy", "Jamaica", "Japan",
            "Jordan", "Kazakhstan", "Kenya", "Kiribati", "Korea, North", "Korea, South", "Kuwait", "Kyrgyzstan", "Laos", "Latvia", "Lebanon",
            "Lesotho", "Liberia", "Libya", "Liechtenstein", "Lithuania", "Luxembourg", "Macedonia", "Madagascar", "Malawi", "Malaysia",
            "Maldives", "Mali", "Malta", "Marshall Islands", "Mauritania", "Mauritius", "Mexico", "Montenegro", "Micronesia", "Moldova",
            "Mongolia", "Morocco", "Monaco", "Mozambique", "Namibia", "Nauru", "Nepal", "Netherlands", "New Zealand", "Nicaragua", "Niger",
            "Nigeria", "Norway", "Oman", "Pakistan", "Panama", "Papua New Guinea", "Paraguay", "Peru", "Philippines", "Poland", "Portugal",
            "Qatar", "Romania", "Russia", "Rwanda", "Samoa", "San Marino", " Sao Tome", "Saudi Arabia", "Senegal", "Serbia", "Seychelles",
            "Sierra Leone", "Singapore", "Slovakia", "Slovenia", "Solomon Islands", "Somalia", "South Africa", "Spain", "Sri Lanka", "Sudan",
            "Suriname", "Swaziland", "Sweden", "Switzerland", "Syria", "Taiwan", "Tajikistan", "Tanzania", "Thailand", "Togo", "Tonga",
            "Trinidad and Tobago", "Tunisia", "Turkey", "Turkmenistan", "Uganda", "Ukraine", "United Arab Emirates", "United Kingdom",
            "United States", "Uruguay", "Uzbekistan", "Vanuatu", "Venezuela", "Vietnam", "Yemen", "Zambia", "Zimbabwe"};

    private String generateRandomString(int length, String characters) {
        StringBuilder buffer = new StringBuilder();
        for (int i = 0; i < length; i++) {
            double index = Math.random() * characters.length();
            buffer.append(characters.charAt((int) index));
        }
        return buffer.toString();
    }

    private String generateRandomCountry() {
        double index = Math.random() * COUNTRIES.length;
        return COUNTRIES[(int) index];
    }

    public String generateRandomId() {
        //let' say up to 7 characters in leading numbers of ID
        int numberOfDigitInId = (int) (Math.random() * 2) + 5;
        return generateRandomString(numberOfDigitInId, NUMBERS) + generateRandomCountry().substring(0, 2).toUpperCase();
    }

    public String generateRandomName() {
        int firstNameLength = (int) (Math.random() * 10);
        String firstName = generateRandomString(firstNameLength, ALPHABET);

        int lastNameLength = (int) (Math.random() * 15);
        String lastName = generateRandomString(lastNameLength, ALPHABET);

        return firstName.concat(" ").concat(lastName);
    }

    public double generateRandomGPA() {
        return 3.5 * Math.random() + 0.5;
    }

    public Student generateRandomStudent() {
        Student student = new Student(generateRandomId(), generateRandomName(), generateRandomGPA());
        return student;
    }

    public List<Student> generateRandomStudentList(int numberOfStudents) {
        List<Student> studentList = new ArrayList<>();
        for (int i = 0; i < numberOfStudents; i++) {
            Student student = generateRandomStudent();
            studentList.add(student);
        }
        return studentList;
    }

}
