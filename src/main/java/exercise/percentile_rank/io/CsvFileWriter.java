package exercise.percentile_rank.io;

import exercise.percentile_rank.pojo.Student;
import exercise.percentile_rank.util.RandomGenerator;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVPrinter;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class CsvFileWriter {

    //Delimiter used in CSV file
    private final String NEW_LINE_SEPARATOR = "\n";

    public void writeCsvFile(File file, int numberOfStudents) throws IOException {
        List<Student> students = new RandomGenerator().generateRandomStudentList(numberOfStudents);

        FileWriter fileWriter = null;

        CSVPrinter csvFilePrinter = null;
        //Create the CSVFormat object with "\n" as a record delimiter
        CSVFormat csvFileFormat = CSVFormat.DEFAULT.withRecordSeparator(NEW_LINE_SEPARATOR);
        try {
            //initialize FileWriter object
            fileWriter = new FileWriter(file);
            //initialize CSVPrinter object
            csvFilePrinter = new CSVPrinter(fileWriter, csvFileFormat);

            //Write a new student object list to the CSV file
            for (Student student : students) {
                List studentDataRecord = new ArrayList();
                studentDataRecord.add(student.getId());
                studentDataRecord.add(student.getName());
                studentDataRecord.add(String.valueOf(student.getGPA()));
                csvFilePrinter.printRecord(studentDataRecord);
            }

            System.out.println("CSV file was created successfully !!!");
        } catch (Exception e) {
            System.out.println("Error in CsvFileWriter !!!");
            e.printStackTrace();
        } finally {
            try {
                assert fileWriter != null;
                fileWriter.flush();
                fileWriter.close();
                assert csvFilePrinter != null;
                csvFilePrinter.close();
            } catch (IOException e) {
                System.out.println("Error while flushing/closing fileWriter/csvPrinter !!!");
                e.printStackTrace();
            }
        }
    }
}