package exercise.percentile_rank.io;

import exercise.percentile_rank.pojo.Student;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class CsvFileReader {

    private final String STUDENT_ID = "id";
    private final String STUDENT_NAME = "name";
    private final String STUDENT_GPA = "gpa";

    public List<Student> readCsvFile(File file)throws Exception{

        long recodedNumber = -1;

        List<Student> students = null;
        FileReader fileReader = null;
        CSVParser csvFileParser = null;

        CSVFormat csvFileFormat = CSVFormat.DEFAULT.withHeader(STUDENT_ID, STUDENT_NAME, STUDENT_GPA).withQuote(null);
        try {
            //Create a new list of student to be filled by CSV file data
            students = new ArrayList<>();

            //initialize FileReader object
            fileReader = new FileReader(file);

            //initialize CSVParser object
            csvFileParser = new CSVParser(fileReader, csvFileFormat);

            //Get a list of CSV file records
            List<CSVRecord> csvRecords = csvFileParser.getRecords();

            //Read the CSV file records starting from the second record to skip the header
            for (CSVRecord record : csvRecords) {
                //save number of record
                recodedNumber = record.getRecordNumber();
                String id = record.get(STUDENT_ID);
                String name = record.get(STUDENT_NAME);
                String gpa = record.get(STUDENT_GPA);
                //Create a new student object and fill his data
                Student student = new Student(id, name, Double.parseDouble(gpa));
                students.add(student);
            }
        } catch (IOException e){
            e.printStackTrace();
            throw new IOException(e);
        } catch (IllegalArgumentException e) {
            System.out.println("Error in CsvFileReader. Error in line: " + recodedNumber);
            throw new IllegalArgumentException();
        } finally {
            try {
                assert fileReader != null;
                fileReader.close();
                assert csvFileParser != null;
                csvFileParser.close();
            } catch (IOException e) {
                System.out.println("Error while closing fileReader/csvFileParser");
                e.printStackTrace();
            }
        }
        return students;
    }

}