package exercise.percentile_rank.service;

import exercise.percentile_rank.pojo.Student;
import exercise.percentile_rank.util.StudentListHelper;

import java.util.Comparator;
import java.util.List;

public class Percentile {

    private List<Student> studentList;

    public void init(List<Student> list) {
        if (list.size() == 0) {
            throw new IllegalArgumentException("List is empty");
        }
        list.sort(Comparator.comparingDouble(Student::getGPA));
        setStudentList(list);
    }

    public double rank(Student student) {
        if (getStudentList().size() == 0) {
            throw new IllegalArgumentException("List is empty");
        }

        int n = getStudentList().size();

        int sameCount = StudentListHelper.countSameGPA(getStudentList(), student.getGPA());
        int lowerCount = StudentListHelper.countLessThenGPA(getStudentList(), student.getGPA());

        if (sameCount == 0) {
            throw new IllegalArgumentException("Value do not exists in list");
        }

        return (lowerCount + 0.5 * sameCount) / n * 100;
    }

    public List<Student> getStudentList() {
        return studentList;
    }

    public void setStudentList(List<Student> studentList) {
        this.studentList = studentList;
    }

}